'use strict';

const H5P = require('@lumieducation/h5p-server');
const createH5PEditor = require('./createH5PEditor.js');

const {
    h5pAjaxExpressRouter,
    libraryAdministrationExpressRouter,
    contentTypeCacheExpressRouter,
    IRequestWithUser
} = require('@lumieducation/h5p-express');

const startPageRenderer = require('./startPageRenderer');

var Router = require('koa-router');
var c2k = require('koa-connect');
const serve = require('koa-static')
const path = require('path')
const mount = require('koa-mount')
/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

module.exports = async () => {
    const config = await new H5P.H5PConfig(
        new H5P.fsImplementations.JsonStorage(
            path.join(__dirname, '../config.json')
        )
    ).load();

    const h5pEditor = await createH5PEditor(
        config,
        path.join(__dirname, '../h5p/libraries'), // the path on the local disc where
        // libraries should be stored)
        path.join(__dirname, '../h5p/content'), // the path on the local disc where content
        // is stored. Only used / necessary if you use the local filesystem
        // content storage class.
        path.join(__dirname, '../h5p/temporary-storage'), // the path on the local disc
        // where temporary files (uploads) should be stored. Only used /
        // necessary if you use the local filesystem temporary storage class.
        (key, language) => translationFunction(key, { lng: language })
    );

    // The H5PPlayer object is used to display H5P content.
    const h5pPlayer = new H5P.H5PPlayer(
        h5pEditor.libraryStorage,
        h5pEditor.contentStorage,
        config
    );

    // strapi.app.use(
    //     h5pEditor.config.baseUrl,
    //     h5pAjaxExpressRouter(
    //         h5pEditor,
    //         path.resolve(path.join(__dirname, '../h5p/core')), // the path on the local disc where the
    //         // files of the JavaScript client of the player are stored
    //         path.resolve(path.join(__dirname, '../h5p/editor')), // the path on the local disc where the
    //         // files of the JavaScript client of the editor are stored
    //         undefined,
    //         'auto' // You can change the language of the editor here by setting
    //         // the language code you need here. 'auto' means the route will try
    //         // to use the language detected by the i18next language detector.
    //     )
    // );

    var router = new Router();
    strapi.app.use(router.routes()).use(router.allowedMethods());
    console.log(h5pEditor);
    router.get(h5pEditor.config.baseUrl, c2k(h5pAjaxExpressRouter(
        h5pEditor,
        path.resolve(path.join(__dirname, '../h5p/core')), // the path on the local disc where the
        // files of the JavaScript client of the player are stored
        path.resolve(path.join(__dirname, '../h5p/editor')), // the path on the local disc where the
        // files of the JavaScript client of the editor are stored
        undefined,
        'auto' // You can change the language of the editor here by setting
        // the language code you need here. 'auto' means the route will try
        // to use the language detected by the i18next language detector.
    )))

    console.log(libraryAdministrationExpressRouter(h5pEditor));
    router.use(
        `${h5pEditor.config.baseUrl}/libraries`,
        c2k(libraryAdministrationExpressRouter(h5pEditor))
    );

    // strapi.app.use(c2k(libraryAdministrationExpressRouter(h5pEditor)));
    router.get(h5pEditor.config.baseUrl, c2k(startPageRenderer(h5pEditor)));
    // server.use('/client', express.static(path.join(__dirname, 'build/client')));
    // strapi.app.use(serve(path.resolve(__dirname, "build/client")));
    console.log(__dirname, path.join(__dirname, '../../build/client'))
    strapi.app.use(mount('/client', serve(path.join(__dirname, '../../build/client'))))

};
