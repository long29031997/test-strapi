module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql2',
        host: 'localhost',
        port: 3306,
        username: 'strapi',
        password: 'strapi123',
        database: 'strapi',
      },
      options: {
      },
    },
  },
});
